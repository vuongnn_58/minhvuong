# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table product (
  id                        bigint not null,
  ean                       varchar(255),
  name                      varchar(255),
  description               varchar(255),
  picture                   varbinary(255),
  constraint pk_product primary key (id))
;

create sequence product_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists product;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists product_seq;

# --- Initialize tags

# --- !Ups
insert info tag(id,name) value (1, 'lightweight');
insert info tag(id,name) value (2, 'metal');
insert info tag(id,name) value (3, 'plastic');

# ---!Downs

SET REFERENTIAL_INTEGRITY FALSE;

delete from tag;