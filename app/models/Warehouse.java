package models;


import java.util.ArrayList;
import java.util.List;
import play.db.ebean.Model;
import javax.persistence.*;

@Entity
public class Warehouse extends Model {
    @Id
    public Long id;
 
    public String name;
    public Address address;
    
    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList();
 
    public String toString() {
        return name;
    }
}