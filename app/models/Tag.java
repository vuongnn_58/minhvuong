package models;
 
import play.data.validation.Constraints;
import java.util.*;
 
public class Tag extends Model{
    public Long id;
    @Constraints.Required
    public String name;
    public List<Product> products;
    public static Finder<Long, Tag> find =
               new Finder<>(Long.class, Tag.class);
   public static Tag findById(Long id) {
      return find.byId(id);
   }
    
    public static Tag findById(Long id) {
        for (Tag tag: tags) {
            if (tag.id == id) return tag;
        }
        return null;
    }
    
    public Tag(){
        // Left empty
    }
    public Tag(Long id, String name, Collection<Product> products) {
        this.id = id;
        this.name = name;
        this.products = new LinkedList<Product>(products);
        for (Product product : products) {
            product.tags.add(this);
        }
    }
    
}