package models;

import javax.persistence.*;

@Entity
public class StockItem extends Model{
    @Id
    public Long id;
 
    public Warehouse warehouse;
 
    @ManyToOne
    public Product product;
 
    public Long quantity;
 
    public String toString() {
        return String.format("StockItem %d - %d x product %s",
            id, quantity, product == null ? null : product.id);
  }
}